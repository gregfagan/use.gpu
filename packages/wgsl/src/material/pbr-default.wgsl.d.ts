declare module "@use-gpu/wgsl/material/pbr-default.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getDefaultPBRMaterial: ParsedBundle;
  export default __module;
}
