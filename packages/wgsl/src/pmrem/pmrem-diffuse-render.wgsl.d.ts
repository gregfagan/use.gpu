declare module "@use-gpu/wgsl/pmrem/pmrem-diffuse-render.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const pmremDiffuseRender: ParsedBundle;
  export default __module;
}
