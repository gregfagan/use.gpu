declare module "@use-gpu/wgsl/pmrem/pmrem-init.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const pmremInit: ParsedBundle;
  export default __module;
}
