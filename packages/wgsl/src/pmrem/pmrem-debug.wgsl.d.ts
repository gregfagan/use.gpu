declare module "@use-gpu/wgsl/pmrem/pmrem-debug.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const pmremGridOverlay: ParsedBundle;
  export default __module;
}
