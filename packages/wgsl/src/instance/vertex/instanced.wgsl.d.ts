declare module "@use-gpu/wgsl/instance/vertex/instanced.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getInstancedVertex: ParsedBundle;
  export default __module;
}
