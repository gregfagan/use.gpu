declare module "@use-gpu/wgsl/instance/vertex/arrow.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getArrowVertex: ParsedBundle;
  export default __module;
}
