declare module "@use-gpu/wgsl/instance/vertex/sdf-rectangle.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getSDFRectangleVertex: ParsedBundle;
  export default __module;
}
