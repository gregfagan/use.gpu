declare module "@use-gpu/wgsl/instance/index/face.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getInstancedFaceIndex: ParsedBundle;
  export default __module;
}
