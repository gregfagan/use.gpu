declare module "@use-gpu/wgsl/instance/index/interleave.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getInterleaveIndex: ParsedBundle;
  export default __module;
}
