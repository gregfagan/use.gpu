declare module "@use-gpu/wgsl/plot/surface-normal.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getSurfaceNormal: ParsedBundle;
  export default __module;
}
