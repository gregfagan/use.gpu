declare module "@use-gpu/wgsl/render/fragment/ui.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const main: ParsedBundle;
  export default __module;
}
